.. shellscript-notes documentation master file, created by
   sphinx-quickstart on Sun Mar 13 10:49:38 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to shellscript-notes's documentation!
=============================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   cpp/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
